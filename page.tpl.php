<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
	<div id="head" class="clear-block">		
			<?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
			    <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
			<!--<?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
	  
	      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
		</div>-->
	</div>
		<?php if ($sidebar_left) { ?>
			<div id="sidebar_left"><?php print $sidebar_left ?></div>
		<?php } ?>
		<?php if ($sidebar_right) { ?>
			<div id="sidebar_right"><?php print $sidebar_right ?></div>
		<?php } ?>
		<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      
		<div id="content" class="content">
					<h1 class="title"><?php print $title ?></h1>
					<?php if ($breadcrumb) { ?>
					<div id="breadcrumbs">
				        <?php print $breadcrumb ?>
					</div>
					<?php } ?>
	        <div class="tabs"><?php print $tabs ?></div>

	        <?php print $help ?>
	        <?php print $messages ?>
	        <?php print $content; ?>
	        <?php print $feed_icons; ?>
					<br style="clear:both" />
					<div id="footer">
				  <?php print $footer_message ?><br />
					<?php print $closure ?>

					<?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?><?php } ?>
					</div>
		</div>


		<?php if (($sidebar_right) || ($sidebar_left)) { ?>
		<br style="clear:both">
		<?php } ?>




</body>
</html>